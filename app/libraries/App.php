<?php

class App {
  
  protected $currentController = 'Passwords';
  protected $currentMethod = 'index';
  protected $params = [];

  public function __construct()
  {
    $url = $this->getUrl();
    unset($url[0]);

    if (isset($url[1]) && file_exists('../app/controllers/' . ucwords($url[1]) . '.php')) {
      $this->currentController = ucwords($url[1]);
      unset($url[1]);
    }

    require_once '../app/controllers/' . $this->currentController . '.php';

    $this->currentController = new $this->currentController;

    if (isset($url[2])) {
      if (method_exists($this->currentController, $url[2])) {
        $this->currentMethod = $url[2];
        unset($url[2]);
      }
    }

    $this->params = $url ? array_values($url) : [];

    call_user_func([$this->currentController, $this->currentMethod], $this->params);

  }

  public function getUrl()
  {    
    if (isset($_SERVER['REQUEST_URI'])) {
      $url = filter_var(rtrim($_SERVER['REQUEST_URI'], '/'), FILTER_SANITIZE_URL);
      return explode('/', $url);
    }
  }
}

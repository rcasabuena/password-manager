<?php

class Password {

  private $db;

  public function __construct()
  {
    $this->db = new Database;
  }

  public function getUserPasswords($user_id) 
  {
    $this->db->query('SELECT * FROM passwords WHERE user_id = :user_id');
    $this->db->bind(':user_id', $user_id);
    $results = $this->db->results();
    return $results;
  }

  public function find($id) 
  {
    $this->db->query('SELECT * FROM passwords WHERE id = :id');
    $this->db->bind(':id', $id);
    return $this->db->single();
  }

  public function create($user_id, $form) 
  {
    $this->db->query('INSERT INTO passwords (url, password, user_id) VALUES (:url, :password, :user_id)');
    $this->db->bind(':url', $form['url']);
    $this->db->bind(':password', password_hash($form['password'], PASSWORD_DEFAULT));
    $this->db->bind(':user_id', $user_id);
    return $this->db->execute();
  }

  public function update($id, $form) 
  {
    $this->db->query('UPDATE passwords SET url = :url, password = :password WHERE id = :id');
    $this->db->bind(':url', $form['url']);
    $this->db->bind(':password', password_hash($form['password'], PASSWORD_DEFAULT));
    $this->db->bind(':id', $id);
    return $this->db->execute();
  }

  public function delete($id) 
  {
    $this->db->query('DELETE FROM passwords WHERE id = :id');
    $this->db->bind(':id', $id);
    $this->db->execute();
  }
}
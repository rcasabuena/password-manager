<?php

class User {

  private $db;

  public function __construct()
  {
    $this->db = new Database;
  }

  public function login($username, $password) 
  {    
    $this->db->query('SELECT * FROM users WHERE username = :username');
    $this->db->bind(':username', $username);

    if ($row = $this->db->single()) {
      $hashedPassword = $row->password;
      if (password_verify($password, $hashedPassword)) {
        return $row;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
<?php
   require APP_ROOT . '/views/includes/head.php';
?>
<div>
  <?php
    require APP_ROOT . '/views/includes/nav.php';
  ?>

  <div class="container-password">
    <h2>Edit URL/Password</h2>
    <form method="POST" id="loginForm">
      <label for="url">
        <span>
          URL *
          <?php if ($data['errors']['url'] != '') : ?>
            <span class="error-message"><?=$data['errors']['url']?></span>
          <?php endif; ?>
        </span>
        <input 
          <?php if ($data['errors']['url'] != '') : ?>
            class="input-error"
          <?php endif; ?>
          type="text" 
          name="url" 
        />
      </label>
      <label for="password">
        <span>
          Password *
          <?php if ($data['errors']['password'] != '') : ?>
            <span class="error-message"><?=$data['errors']['password']?></span>
          <?php endif; ?>
        </span>
        <input 
          <?php if ($data['errors']['password'] != '') : ?>
            class="input-error"
          <?php endif; ?>
          type="password" 
          name="password" 
        />
      </label>
      <input class="btn" type="submit" value="Create">
      <em class="info">* Required</em>
    </form>
  </div>
</div>

<?php
   require APP_ROOT . '/views/includes/foot.php';
?>
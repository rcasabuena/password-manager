<?php
   require APP_ROOT . '/views/includes/head.php';
?>
<div>
  <?php
    require APP_ROOT . '/views/includes/nav.php';
  ?>

  <div class="container-password">
    <a class="btn mb1" href="/passwords/new">Add New URL/Password</a>
    <?php if (count($data['passwords']) > 0): ?>
      <table class="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>URL</th>
            <th>Password</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($data['passwords'] as $password): ?>
            <tr>
              <td><?=$password->id?></td>
              <td><?=$password->url?></td>
              <td><input type="password" disabled value="**********" /></td>
              <td><a href="/passwords/edit/<?=$password->id?>">Edit</a></td>
              <td><a class="confirm" href="/passwords/delete/<?=$password->id?>">Delete</a></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h3>You have no passwords stored yet.</h3>
    <?php endif; ?>
  </div>
</div>

<?php
   require APP_ROOT . '/views/includes/foot.php';
?>
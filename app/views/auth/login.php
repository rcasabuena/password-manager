<?php
   require APP_ROOT . '/views/includes/head.php';
?>
<div class="container-login">
  <form method="POST" id="loginForm">
    <?php if ($data['errors']['login'] != '') : ?>
      <span class="error-message pb1"><?=$data['errors']['login']?></span>
    <?php endif; ?>
    <label for="username">
      <span>
        Username *
        <?php if ($data['errors']['username'] != '') : ?>
          <span class="error-message"><?=$data['errors']['username']?></span>
        <?php endif; ?>
      </span>
      <input 
        <?php if ($data['errors']['username'] != '') : ?>
          class="input-error"
        <?php endif; ?>
        type="text" 
        name="username" 
      />
    </label>
    <label for="password">
      <span>
        Password *
        <?php if ($data['errors']['password'] != '') : ?>
          <span class="error-message"><?=$data['errors']['password']?></span>
        <?php endif; ?>
      </span>
      <input 
        <?php if ($data['errors']['password'] != '') : ?>
          class="input-error"
        <?php endif; ?>
        type="password" 
        name="password" 
      />
    </label>
    <input class="btn" type="submit" value="Login">
    <em class="info">* Required</em>
  </form>
</div>

<?php
   require APP_ROOT . '/views/includes/foot.php';
?>
<nav>
  <?php if (isset($_SESSION['user_id'])) : ?>
    <h3><?=ucwords($_SESSION['username'])?> Password Manager</h3>
  <?php endif; ?>
  <?php if (isset($_SESSION['user_id'])): ?>
      <a class="btn" href="<?=URL_ROOT?>/auth/logout">Logout</a>
  <?php else: ?>
      <a class="btn" href="<?=URL_ROOT?>/auth/login">Login</a>
  <?php endif; ?>
</nav>
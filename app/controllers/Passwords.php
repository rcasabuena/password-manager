<?php

class Passwords extends Controller {

  public function __construct()
  {
    $this->passwordModel = $this->model('Password');
  }

  public function index() 
  {    
    if (isLoggedIn()) {
      $data = [
        'passwords' => $this->passwordModel->getUserPasswords($_SESSION['user_id'])
      ];
      $this->view('passwords/index', $data);
    } else {
      header('location:' . URL_ROOT . '/auth/login');
    }
  }

  public function new() 
  {
    $data = [
      'url' => '',
      'password' => '',
      'errors' => [
        'url' => '',
        'password' => ''
      ]
    ];

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

      $data = [
        'url' => trim($_POST['url']),
        'password' => trim($_POST['password']),
        'errors' => [
          'url' => '',
          'password' => '',
        ]
      ];

      if (trim($data['url']) == '') {
        $data['errors']['url'] = "Please enter your URL.";
      }

      if (trim($data['password']) == '') {
        $data['errors']['password'] = "Please enter your password.";
      }

      if (trim($data['url']) ==! '' && trim($data['password']) ==! '') {
        $this->passwordModel->create($_SESSION['user_id'], $data);
        header('location:' . URL_ROOT);
      } 
    }
    
    $this->view('passwords/new', $data);
  }

  public function edit($params) 
  {
    [$password_id] = $params;

    $data = [
      'item' => $this->passwordModel->find(intval($password_id)),
      'url' => '',
      'password' => '',
      'errors' => [
        'url' => '',
        'password' => ''
      ]
    ];

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

      $data = [
        'item' => $this->passwordModel->find(intval($password_id)),
        'url' => trim($_POST['url']),
        'password' => trim($_POST['password']),
        'errors' => [
          'url' => '',
          'password' => '',
        ]
      ];

      if (trim($data['url']) == '') {
        $data['errors']['url'] = "Please enter your URL.";
      }

      if (trim($data['password']) == '') {
        $data['errors']['password'] = "Please enter your password.";
      }

      if (trim($data['url']) ==! '' && trim($data['password']) ==! '') {
        $this->passwordModel->update($password_id, $data);
        header('location:' . URL_ROOT);
      } 
    }

    $this->view('passwords/edit', $data);
  }

  public function delete($params) 
  {
    [$password_id] = $params;
    $this->passwordModel->delete(intval($password_id));

    header('location:' . URL_ROOT);
  }
}
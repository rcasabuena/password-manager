<?php

class Auth extends Controller {

  public function __construct()
  {
    $this->userModel = $this->model('User');
  }

  public function login() 
  {        
    $data = [
      'errors' => [
        'username' => '',
        'password' => '',
        'login' => ''
      ]
    ];
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

      $data = [
        'username' => trim($_POST['username']),
        'password' => trim($_POST['password']),
        'errors' => [
          'username' => '',
          'password' => '',
          'login' => ''
        ]
      ];

      if ($data['username'] == '') {
        $data['errors']['username'] = "Please enter your username.";
      }

      if ($data['password'] == '') {
        $data['errors']['password'] = "Please enter your password.";
      }

      if (trim($data['username']) ==! '' && trim($data['password']) ==! '') {
        if (!$user = $this->userModel->login($data['username'], $data['password'])) {
          $data['errors']['login'] = "Please enter a valid username or password.";
        } else {
          $this->createUserSession($user);
        }
      } 
    }

    $this->view('auth/login', $data);
  }

  public function createUserSession($user) 
  {
    $_SESSION['user_id'] =  $user->id;
    $_SESSION['username'] =  $user->username;
    header('location:' . URL_ROOT);
  }

  public function logout() 
  {        
    unset($_SESSION['user_id']);
    unset($_SESSION['username']);
    header('location:' . URL_ROOT . '/auth/login');
  }
}
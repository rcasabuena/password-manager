<?php

require_once 'libraries/App.php';
require_once 'libraries/Controller.php';
require_once 'libraries/Database.php';

require_once 'helpers/session.php';

require_once 'config/database.php';
require_once 'config/app.php';

$app = new App;

## About The Test Project

To execute test project:

- Download repo and copy the folder to your Valet "parked" directory.
- Import the sql file to your local mysql installation.
- Update the app/config/app.php file with your local settings.
- Update the app/config/database.php file with your local settings.

- username: user1
- password: password

Scope of test project:

- User can login/logout.
- Logged in user can see all url/password entries in table format.
- Logged in user can see all create/edit/delete url/password entries.
